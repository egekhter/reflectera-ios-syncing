//
// Syncing Services
//

var exec = require("cordova/exec");

var PLUGIN_NAME = 'SyncingService';

var syncingService = {
    name: PLUGIN_NAME,

    // error enum
    UUIDDATA_ERROR: 0, // server responded with invalid uuid_data
    NETWORK_ERROR: 1, // network error
    AUTHORIZATION_ERROR: 2, // 401: user logged out or session expired
    PLANUPGRADE_ERROR: 3, // 426: user exceeded his plan

    _isSyncing: false,
    _totalItems: 0,
    _completedItems: 0,
    _eventHandlers: {},
    _lastError: null,

    lastError: function () {
        return this._lastError;
    },

    totalItems: function () {
        return this._totalItems;
    },

    completedItems: function () {
        return this._completedItems;
    },

    isSyncing: function () {
        return this._isSyncing;
    },

    start: function (archiveId, deviceUUID, fn) {
        exec(fn, null, PLUGIN_NAME, 'start', [ archiveId, deviceUUID ]);
    },

    cancel: function (fn) {
        exec(fn, null, PLUGIN_NAME, 'cancel', []);
    },

    setAllowsCellularAccess: function (flag, fn) {
        exec(fn, null, PLUGIN_NAME, 'setAllowsCellularAccess', [ flag ]);
    },

    allowsCellularAccess: function (fn) {
        exec(fn, null, PLUGIN_NAME, 'allowsCellularAccess', []);
    },

    on: function (event, callback) {
        this._eventHandlers[event] = callback;
    },

    off: function (event) {
        if(this._eventHandlers.hasOwnProperty(event)) {
            delete this._eventHandlers[event];
        }
    },

    _fire: function () {
        var args = Array.prototype.slice.call(arguments);
        var event = args.shift();
        var callback = this._eventHandlers[event]
        if(typeof(callback) === 'function') {
            callback.apply(null, args);
        }
    },

    // Private methods used by native counterpart

    _onSyncStart: function () {
        this._isSyncing = true;
        this._lastError = null;

        console.log('JS: Sync started.');

        this._fire( 'start' );
    },

    _onSyncStop: function () {
        this._isSyncing = false;

        console.log('JS: Sync stopped.');

        this._fire( 'stop' );
    },

    _onSyncCancel: function () {
        console.log('JS: Sync cancelled.');

        this._fire( 'cancel' );
    },

    _onSyncError: function (code, message) {
        var err = new Error();
        err.name = 'SyncError';
        err.number = code;
        err.message = message;

        this._lastError = err;

        console.log('JS: Sync error. Code = ' + code + ', message = ' + message);

        this._fire( 'error', code, message );
    },

    _onSyncProgress: function (completed, total) {
        this._completedItems = completed;
        this._totalItems = total;

        console.log('JS: Sync update ' + completed + ' out of ' + total + '.');

        this._fire( 'progress', completed, total );
    }
};

module.exports = syncingService;
