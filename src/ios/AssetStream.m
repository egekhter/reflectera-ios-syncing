//
//  AssetStream.m
//  Reflectera
//
//  Created by pronebird on 23/01/15.
//
//

#import "AssetStream.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface NSStream ()

@property (readwrite) NSStreamStatus streamStatus;
@property (readwrite, copy) NSError *streamError;

@end

@interface AssetStream ()

@property (strong) ALAsset* asset;
@property (strong) ALAssetRepresentation* assetRepresentation;

@end

@implementation AssetStream {
    long long _totalBytes;
    long long _readBytes;
}

@synthesize streamError;
@synthesize streamStatus;

+ (instancetype)inputStreamWithAsset:(ALAsset*)asset {
    return [[self alloc] initWithAsset:asset];
}

- (id)initWithAsset:(ALAsset*)asset {
    if(self = [super init]) {
        self.asset = asset;
        self.assetRepresentation = [asset defaultRepresentation];
        _totalBytes = [self.assetRepresentation size];
        _readBytes = 0;
    }
    return self;
}

#pragma mark - NSStream subclass overrides

- (void)open {
    self.streamStatus = NSStreamStatusOpen;
}

- (void)close {
    self.streamStatus = NSStreamStatusClosed;
}

- (void)scheduleInRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode {
    // Nothing to do here, because this stream does not need a run loop to produce its data.
}

- (void)removeFromRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode {
    // Nothing to do here, because this stream does not need a run loop to produce its data.
}

- (id)propertyForKey:(NSString *)key {
    return nil;
}

- (BOOL)setProperty:(id)property forKey:(NSString *)key {
    return NO;
}

- (BOOL)getBuffer:(uint8_t **)buffer length:(NSUInteger *)len{
    return NO;
}

- (BOOL)hasBytesAvailable {
    return _readBytes < _totalBytes;
}

- (NSInteger)read:(uint8_t *)buffer maxLength:(NSUInteger)len {
    if(_readBytes >= _totalBytes) {
        return 0;
    }
    
    NSError* error;
    NSUInteger bytesReturned = [self.assetRepresentation getBytes:buffer fromOffset:_readBytes length:len error:&error];
    
    // Check errors
    if(bytesReturned == 0) {
        NSLog(@"Error reading asset: %@", error);
        self.streamStatus = NSStreamStatusError;
        self.streamError = error;
    }
    
    _readBytes += bytesReturned;
    
    // Update stream status when it's consumed
    if(_readBytes >= _totalBytes) {
        self.streamStatus = NSStreamStatusAtEnd;
    }
    
    return bytesReturned;
}

@end
