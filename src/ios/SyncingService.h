//
//  SyncingService.h
//  Copyright (c) 2015 Andrej Mihajlov
//

#import "Foundation/Foundation.h"
#import <Cordova/CDVPlugin.h>

@interface SyncingService : CDVPlugin

- (void)handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler;
- (void)start:(CDVInvokedUrlCommand*)command;
- (void)cancel:(CDVInvokedUrlCommand*)command;
- (void)setAllowsCellularAccess:(CDVInvokedUrlCommand*)command;
- (void)allowsCellularAccess:(CDVInvokedUrlCommand*)command;

@end
