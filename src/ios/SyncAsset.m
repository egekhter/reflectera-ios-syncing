//
//  SyncAsset.m
//  Reflectera
//
//  Created by pronebird on 22/01/15.
//
//

#import "SyncAsset.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>

static NSString* const kSyncAssetGroupsKey = @"groups";
static NSString* const kSyncAssetURLKey = @"assetURL";
static NSString* const kSyncAssetExtensionKey = @"assetExtension";
static NSString* const kSyncAssetIDKey = @"assetID";
static NSString* const kSyncAssetMimeTypeKey = @"mimeType";
static NSString *const kSyncAssetKindKey = @"kind";
static NSString *const kSyncAssetSizeKey = @"size";
static NSString *const kSyncAssetRetryCountKey = @"retryCount";

static NSDictionary* SyncItemParseQuery(NSURL* url) {
    NSString* assetURLString = [url absoluteString];
    NSRange queryRange = [assetURLString rangeOfString:@"?"];
    
    if(queryRange.location == NSNotFound) {
        return nil;
    }
    
    NSString* assetURLQuery = [assetURLString substringFromIndex:queryRange.location+1];
    NSArray* queryItems = [assetURLQuery componentsSeparatedByString:@"&"];
    NSMutableDictionary* dictionary = [NSMutableDictionary new];
    
    for(NSString* str in queryItems) {
        NSArray* split = [str componentsSeparatedByString:@"="];
        NSString* key = [split firstObject];
        NSString* value = [split lastObject];
        
        dictionary[key] = value;
    }
    
    return dictionary;
}

@interface SyncAsset()

@property (strong, readwrite) NSMutableArray* groupsArray;

@end

@implementation SyncAsset

+ (NSString*)assetIDFromURL:(NSURL *)url {
    NSDictionary* dictionary = SyncItemParseQuery(url);
    return dictionary[@"id"];
}

+ (NSString*)assetExtensionFromURL:(NSURL *)url {
    NSDictionary* dictionary = SyncItemParseQuery(url);
    return dictionary[@"ext"];
}

+ (NSString*)mimeTypeForAsset:(ALAsset*)asset; {
    ALAssetRepresentation *rep = [asset defaultRepresentation];
    
    NSString* MIMEType = (__bridge_transfer NSString*)UTTypeCopyPreferredTagWithClass((__bridge CFStringRef)[rep UTI], kUTTagClassMIMEType);
    
    return MIMEType;
}

- (NSArray*)groups {
    return [NSArray arrayWithArray:self.groupsArray];
}

- (void)addGroup:(NSString*)group {
    if(![self.groupsArray containsObject:group]) {
        [self.groupsArray addObject:group];
    }
}

- (NSString*)fileName {
    return [self.assetID stringByAppendingPathExtension:self.assetExtension];
}

- (id)init {
    if(self = [super init]) {
        self.groupsArray = [NSMutableArray new];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.groupsArray forKey:kSyncAssetGroupsKey];
    [aCoder encodeObject:self.assetURL forKey:kSyncAssetURLKey];
    [aCoder encodeObject:self.assetID forKey:kSyncAssetIDKey];
    [aCoder encodeObject:self.assetExtension forKey:kSyncAssetExtensionKey];
    [aCoder encodeObject:self.mimeType forKey:kSyncAssetMimeTypeKey];
    [aCoder encodeInteger:self.kind forKey:kSyncAssetKindKey];
    [aCoder encodeInt64:self.size forKey:kSyncAssetSizeKey];
    [aCoder encodeInteger:self.retryCount forKey:kSyncAssetRetryCountKey];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if(self = [super init]) {
        self.groupsArray = [[aDecoder decodeObjectForKey:kSyncAssetGroupsKey] mutableCopy];
        self.assetURL = [aDecoder decodeObjectForKey:kSyncAssetURLKey];
        self.assetID = [aDecoder decodeObjectForKey:kSyncAssetIDKey];
        self.assetExtension = [aDecoder decodeObjectForKey:kSyncAssetExtensionKey];
        self.mimeType = [aDecoder decodeObjectForKey:kSyncAssetMimeTypeKey];
        self.kind = [aDecoder decodeIntegerForKey:kSyncAssetKindKey];
        self.size = [aDecoder decodeInt64ForKey:kSyncAssetSizeKey];
        self.retryCount = [aDecoder decodeIntegerForKey:kSyncAssetRetryCountKey];
    }
    return self;
}

- (NSString*)description {
    return [NSString stringWithFormat:@"%@ { groups = %@, assetURL = %@, id = %@, ext = %@, mimeType = %@, size = %lld }", [super description], self.groupsArray, [self.assetURL absoluteString], self.assetID, self.assetExtension, self.mimeType, self.size];
}

@end
