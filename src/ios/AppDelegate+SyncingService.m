//
//  AppDelegate+SyncingService.m
//  Reflectera
//
//  Created by pronebird on 23/01/15.
//
//

#import "AppDelegate+SyncingService.h"
#import "AppDelegate.h"
#import "SyncingService.h"

@implementation AppDelegate (SyncingService)

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler {
    SyncingService* syncingService = [self.viewController getCommandInstance:@"SyncingService"];
    [syncingService handleEventsForBackgroundURLSession:identifier completionHandler:completionHandler];
}

@end
