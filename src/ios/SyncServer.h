//
//  SyncServer.h
//  Reflectera
//
//  Created by pronebird on 22/01/15.
//
//

#import <Foundation/Foundation.h>
#import "SyncServerDelegate.h"

extern NSString* const SyncServerErrorDomain;

typedef NS_ENUM(NSInteger, SyncServerError) {
    SyncServerErrorInvalidUUIDData,
    SyncServerErrorNetworkError,
    SyncServerErrorAuthorizationFailed,
    SyncServerErrorPlanUpgradeRequired
};

@interface SyncServer : NSObject

@property (weak) id<SyncServerDelegate> delegate;

// Allow uploads over cellular network
@property (nonatomic) BOOL allowsCellularAccess;

- (id)initWithDelegate:(id<SyncServerDelegate>)delegate NS_DESIGNATED_INITIALIZER;
- (void)restoreSessionFromCache;

- (void)startWithArchiveId:(NSString*)archiveId deviceUUID:(NSString*)deviceUUID;
- (void)cancel;

- (void)handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler;

@end
