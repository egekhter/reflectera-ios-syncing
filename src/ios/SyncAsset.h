//
//  SyncAsset.h
//  Reflectera
//
//  Created by pronebird on 22/01/15.
//
//

#import <Foundation/Foundation.h>

@class ALAsset;

typedef NS_ENUM(NSInteger, SyncAssetKind) {
    /**
     *  Can be anything that Pictures app cannot consume.
     *  E.g. GIF, OGG, MKV, etc.
     *  This is usually converted from ALAssetTypeUnknown
     */
    SyncAssetKindUnknown = 0,
    
    /**
     *  Photo
     *  This is usually converted from ALAssetTypePhoto
     */
    SyncAssetKindPhoto,
    
    /**
     *  Video
     *  This is usually converted from ALAssetTypeVideo
     */
    SyncAssetKindVideo
};

@interface SyncAsset : NSObject<NSCoding>

/**
 *  An array of names for albums this asset resides in
 */
@property (copy, nonatomic, readonly) NSArray *groups;

/**
 *  Asset ID in Assets Library
 *  This value is extracted from Asset URL.
 */
@property NSString *assetID;

/**
 *  Asset file extension
 */
@property NSString *assetExtension;

/**
 *  Standard mime type
 */
@property NSString *mimeType;

/**
 *  Asset URL in Assets Library
 */
@property NSURL *assetURL;

/**
 *  Type of asset Photo, Video or Unknown
 */
@property SyncAssetKind kind;

/**
 *  Asset size in bytes
 */
@property long long size;

/**
 *  Retry counter
 */
@property NSInteger retryCount;

/**
 *  Returns $UUID.$EXTENSION
 */
@property (readonly, nonatomic) NSString *fileName;

/**
 *  Add album
 *
 *  Essentially adds provided string into SyncAsset.groups and 
 *  makes sure groups contain only unique albums.
 *
 *  @param group an album name
 */
- (void)addGroup:(NSString *)group;

/**
 *  Extract asset ID (UUID) from Asset URL
 *
 *  @param url an asset URL returned from AssetsLibrary
 *
 *  @return an asset ID
 */
+ (NSString *)assetIDFromURL:(NSURL*)url;

/**
 *  Extract asset extension from Asset URL
 *
 *  @param url  an asset URL returned from AssetsLibrary
 *
 *  @return an asset extension (e.g. JPG, MOV)
 */
+ (NSString *)assetExtensionFromURL:(NSURL *)url;

/**
 *  Get standard mime type for ALAsset
 *
 *  @param asset an instance of ALAsset
 *
 *  @return a mime type (e.g. image/jpeg, video/quicktime)
 */
+ (NSString *)mimeTypeForAsset:(ALAsset *)asset;

@end
