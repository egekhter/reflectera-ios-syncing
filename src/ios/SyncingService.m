//
//  SyncingService.m
//  Copyright (c) 2015 Andrej Mihajlov
//

#import "SyncingService.h"
#import <Cordova/CDVDebug.h>

#import "SyncServer.h"
#import "SyncAsset.h"

static void* kSyncProgressContext = &kSyncProgressContext;
static void* kSyncTotalCountContext = &kSyncTotalCountContext;

// Plugin private interface
@interface SyncingService() <SyncServerDelegate>

@property (nonatomic, readonly) SyncServer* syncServer;
@property NSProgress* syncProgress;
@property NSError* lastError;
@property BOOL syncing;

@end

// Plugin implementation
@implementation SyncingService

@synthesize syncServer = _syncServer;

- (SyncServer*)syncServer {
    if(!_syncServer) {
        _syncServer = [[SyncServer alloc] initWithDelegate:self];
    }
    return _syncServer;
}

- (void)pluginInitialize {
    [super pluginInitialize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pageDidLoad:) name:CDVPageDidLoadNotification object:self.webView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidFinishLaunching:) name:UIApplicationDidFinishLaunchingNotification object:nil];
}

- (void)appDidFinishLaunching:(NSNotification*)notification {
    if([UIApplication sharedApplication].applicationState != UIApplicationStateBackground) {
        [self.syncServer restoreSessionFromCache];
    }
}

- (void)pageDidLoad:(NSNotification*)notification {
    [self.commandDelegate runInBackground:^{
        if(self.syncing) {
            [self sendSyncStartToJSBridge];
            if(self.syncProgress) {
                [self sendSyncProgressToJSBridge:self.syncProgress];
            }
        }
    }];
}

- (void)handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler {
    [self.syncServer handleEventsForBackgroundURLSession:identifier completionHandler:completionHandler];
}

- (void)start:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult;
        NSString* archiveId = [command argumentAtIndex:0];
        NSString* deviceUUID = [command argumentAtIndex:1];
        
        // convert number to string
        if([archiveId isKindOfClass:NSNumber.class]) {
            archiveId = [(NSNumber*)archiveId stringValue];
        }
        
        // bail if archiveId is not a string
        if(![archiveId isKindOfClass:NSString.class]) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"First parameter expected to be a number or string."];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            return;
        }
        
        if(![deviceUUID isKindOfClass:NSString.class]) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Second parameter expected to be a device UUID string."];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            return;
        }
        
        [self.syncServer startWithArchiveId:archiveId deviceUUID:deviceUUID];
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)cancel:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        
        [self.syncServer cancel];

        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)setAllowsCellularAccess:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult;
        NSNumber* allowsCellularAccess = [command argumentAtIndex:0];

        if([allowsCellularAccess isKindOfClass:NSNumber.class]) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            
            self.syncServer.allowsCellularAccess = allowsCellularAccess.boolValue;
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"First argument is expected to be boolean."];
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)allowsCellularAccess:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:self.syncServer.allowsCellularAccess];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

#pragma mark - JS Bridge

- (void)sendSyncStartToJSBridge {
    NSString* commandJS = @"var fn = function () { syncingService._onSyncStart(); document.removeEventListener('deviceready', fn); }; document.addEventListener('deviceready', fn, false);";
    [self.commandDelegate evalJs:commandJS];
}

- (void)sendSyncStopToJSBridge {
    NSString* commandJS = @"var fn = function () { syncingService._onSyncStop(); document.removeEventListener('deviceready', fn); }; document.addEventListener('deviceready', fn, false);";
    [self.commandDelegate evalJs:commandJS];
}

- (void)sendSyncCancelToJSBridge {
    NSString* commandJS = @"var fn = function () { syncingService._onSyncCancel(); document.removeEventListener('deviceready', fn); }; document.addEventListener('deviceready', fn, false);";
    [self.commandDelegate evalJs:commandJS];
}

- (void)sendSyncProgressToJSBridge:(NSProgress*)progress {
    NSString* commandJS = [NSString stringWithFormat:@"var fn = function () { syncingService._onSyncProgress(%lld, %lld); document.removeEventListener('deviceready', fn); }; document.addEventListener('deviceready', fn, false);", progress.completedUnitCount, progress.totalUnitCount ];
    [self.commandDelegate evalJs:commandJS];
}

- (void)sendSyncErrorToJSBridge:(NSError*)error {
    NSString* message = [error.localizedDescription stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString* commandJS = [NSString stringWithFormat:@"var fn = function () { syncingService._onSyncError(%ld, '%@'); document.removeEventListener('deviceready', fn); }; document.addEventListener('deviceready', fn, false);", (long)error.code, message];
    [self.commandDelegate evalJs:commandJS];
}

#pragma mark - SyncServerDelegate

- (void)syncServerDidStartSyncing:(SyncServer *)server {
    self.syncing = YES;
    [self sendSyncStartToJSBridge];
}

- (void)syncServer:(SyncServer *)server didStartUploadingWithProgress:(NSProgress *)progress {
    self.syncProgress = progress;
    
    [self.syncProgress addObserver:self forKeyPath:@"fractionCompleted" options:NSKeyValueObservingOptionNew context:kSyncProgressContext];
    [self.syncProgress addObserver:self forKeyPath:@"totalUnitCount" options:NSKeyValueObservingOptionNew context:kSyncTotalCountContext];

    // reset initial progress
    [self sendSyncProgressToJSBridge:self.syncProgress];
}

- (void)syncServerDidFinishSyncing:(SyncServer*)server {
    [self.syncProgress removeObserver:self forKeyPath:@"fractionCompleted" context:kSyncProgressContext];
    [self.syncProgress removeObserver:self forKeyPath:@"totalUnitCount" context:kSyncTotalCountContext];
    
    self.syncProgress = nil;
    self.syncing = NO;
    
    [self sendSyncStopToJSBridge];
}

- (void)syncServerDidCancelSyncing:(SyncServer *)server {
    [self sendSyncCancelToJSBridge];
}

- (void)syncServer:(SyncServer *)server didFailWithError:(NSError *)error {
    self.lastError = error;
    
    [self sendSyncErrorToJSBridge:error];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if(context == kSyncProgressContext || context == kSyncTotalCountContext) {
        [self sendSyncProgressToJSBridge:(NSProgress *)object];
        return;
    }
    
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

@end
