//
//  SyncServer.m
//  Reflectera
//
//  Created by pronebird on 22/01/15.
//
//

#import "SyncServer.h"
#import "SyncAsset.h"
#import "AssetStream.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "AFNetworking.h"

// Number of concurrent uploads in foreground
static const NSInteger kMaxConcurrentForegroundUploads = 2;

// Number of concurrent uploads in background
static const NSInteger kMaxConcurrentBackgroundUploads = 1;

// Number of retries to perform before giving up on uploading asset
static const NSInteger kMaxRetryCountForUploads = 3;

NSString* const SyncServerErrorDomain = @"com.reflectera.syncingservice.syncserver";

static NSString* const kSyncServerBackgroundURLSessionID = @"com.reflectera.syncingservice.background-session";
static NSString* const kSyncAPIURL = @"https://reflectera.com/api/v1/";
static NSString* const kSyncServerUploadDataEndpoint = @"users/sync/data";
static NSString* const kSyncServerGetSyncDataEndpoint = @"users/sync/data/{archive_id}/{device_uuid}";

// Cache keys
static NSString* const kSyncCacheBackgroundURLSessionID = @"backgroundURLSessionID";
static NSString* const kSyncCacheAllowsCellularAccess = @"allowsCellularAccess";
static NSString* const kSyncCacheDeviceUUIDKey = @"deviceUUID";
static NSString* const kSyncCacheArchiveIDKey = @"archiveID";
static NSString* const kSyncCacheSyncingKey = @"syncing";
static NSString* const kSyncCacheLastErrorKey = @"lastError";
static NSString* const kSyncCacheTotalItemCountKey = @"totalItemCount";
static NSString* const kSyncCacheCompletedItemCountKey = @"completedItemCount";
static NSString* const kSyncCacheForegroundItemQueueKey = @"foregroundItemQueue";
static NSString* const kSyncCacheBackgroundItemQueueKey = @"backgroundItemQueue";
static NSString* const kSyncCacheForegroundUploadingItemIDsKey = @"foregroundUploadingItemIDs";
static NSString* const kSyncCacheBackgroundUploadingItemIDsKey = @"backgroundUploadingItemIDs";
static NSString* const kSyncCacheSynchronizedItemIDsKey = @"synchronizedItemIDs";

/**
 *  Get temporary location for multipart upload
 *
 *  @param assetID an identifier for SyncAsset
 *
 *  @return a path to temporary directory to store multipart file.
 */
static NSString *REMultipartRequestFilePath(NSString *assetID) {
    return [[NSTemporaryDirectory() stringByAppendingPathComponent:assetID] stringByAppendingPathExtension:@"multipart"];
}

// Private interface
@interface SyncServer()

// session ID
@property (nonatomic) NSString* backgroundURLSessionID;

// device UUID
@property NSString* deviceUUID;

// archive ID
@property NSString* archiveID;

// whether session has been restored on launch
@property BOOL didRestoreSession;

// whether app launched in background to process
// background queue events
@property BOOL didLaunchInBackground;

// is sync in progress?
@property BOOL syncing;

// last error
@property NSError* lastError;

// total number of items scheduled for synchronization
@property NSUInteger totalItemCount;

// completed items count
@property NSUInteger completedItemCount;

// remaining items for synchronization in foreground queue
@property NSMutableArray *foregroundItemQueue;

// remaining items for synchronization in background queue
@property NSMutableArray *backgroundItemQueue;

// items that are currently uploading to server on foreground queue
@property NSMutableSet *foregroundUploadingItemIDs;

// items that are currently uploading to server on background queue
@property NSMutableSet *backgroundUploadingItemIDs;

// items that are already synchronized to server
@property NSMutableDictionary* synchronizedItemIDs;

// foreground upload URL session
@property AFURLSessionManager* foregroundUploadSession;

// background upload URL session
@property AFURLSessionManager* backgroundUploadSession;

// API URL session
@property AFHTTPSessionManager* APISession;

// operation queue for all synchronization routine
@property dispatch_queue_t operationQueue;

// synchronization progress
@property NSProgress* progress;

// completion handlers passed to handleEventsForBackgroundURLSession:completionHandler:
@property NSMutableDictionary *URLSessionCompletionHandlers;

// Background task ID used temporarily for default session configuration
@property UIBackgroundTaskIdentifier backgroundTask;

@end

@implementation SyncServer

+ (ALAssetsLibrary*)sharedAssetsLibrary {
    static ALAssetsLibrary* assetsLibrary;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        assetsLibrary = [ALAssetsLibrary new];
    });
    return assetsLibrary;
}

- (id)initWithDelegate:(id<SyncServerDelegate>)delegate {
    if(self = [super init]) {
        self.delegate = delegate;
        self.operationQueue = dispatch_queue_create("com.reflectera.syncServer.operationQueue", NULL);
        self.foregroundItemQueue = [NSMutableArray new];
        self.backgroundItemQueue = [NSMutableArray new];
        self.foregroundUploadingItemIDs = [NSMutableSet new];
        self.backgroundUploadingItemIDs = [NSMutableSet new];
        self.synchronizedItemIDs = [NSMutableDictionary new];
        
        self.URLSessionCompletionHandlers = [NSMutableDictionary new];
        self.backgroundTask = UIBackgroundTaskInvalid;
        
        // create API session with default configuration
        NSURLSessionConfiguration* sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        // lower timeout to 20s because background task will be killed after 30s when in background.
        sessionConfiguration.timeoutIntervalForRequest = 20;
        
        self.APISession = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kSyncAPIURL] sessionConfiguration:sessionConfiguration];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_handleApplicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    }
    return self;
}

- (void)restoreSessionFromCache {
    // read cache and ensure cache consistency
    dispatch_async(self.operationQueue, ^{
        if(self.didRestoreSession) {
            NSLog(@"Session is already restored from cache.");
            return;
        }
        self.didRestoreSession = YES;
        
        NSLog(@"Restore session from cache.");
        
        // read cache
        [self _readState];
        
        // create upload session
        [self _setupURLSessions];
        
        // check cache consistency
        [self _checkStateConsistency];
        
        // restore progress if needed
        if(self.syncing) {
            self.progress = [NSProgress progressWithTotalUnitCount:self.totalItemCount];
            self.progress.completedUnitCount = self.completedItemCount;
            
            NSLog(@"Restore upload progress.");
            
            // start background execution
            [self _startBackgroundExecutionTask];
            
            // check for camera roll updates when launched in foreground
            if(!self.didLaunchInBackground) {
                [self _checkForCameraRollUpdates];
            }
            
            // send syncStart before resuming uploads
            [self _notifyDelegateOnSyncStart];
            
            // send progress back to delegate
            [self _notifyDelegateOnSyncUpload];
            
            // resume uploads if something went wrong..
            [self _scheduleNextForegroundUploadsBatchIfInForeground];
            [self _scheduleNextBackgroundUploadsBatch];
        }
    });
}

- (void)startWithArchiveId:(NSString*)archiveId deviceUUID:(NSString*)deviceUUID {
    NSParameterAssert(archiveId);
    NSParameterAssert(deviceUUID);
    
    // start background task
    __block UIBackgroundTaskIdentifier backgroundTask;
    backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"-[SyncServer startWithArchiveId:deviceUUID:] expiration handler fired.");
        
        [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
        backgroundTask = UIBackgroundTaskInvalid;
    }];
    
    dispatch_async(self.operationQueue, ^{
        NSAssert( self.didRestoreSession, @"Call restoreSessionFromCache first." );
        
        if(self.syncing) {
            NSLog(@"Synchronization is already in progress...");
            
            // start background execution
            [self _startBackgroundExecutionTask];
            
            // end background task
            [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
            
            return;
        }
        
        // start background execution
        [self _startBackgroundExecutionTask];
        
        // send syncStart to delegate
        [self _notifyDelegateOnSyncStart];
        
        // save device UUID
        self.deviceUUID = deviceUUID;
        
        // save archive ID
        self.archiveID = archiveId;
        
        // mark sync as started.
        self.syncing = YES;
        
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        __block NSError* syncDataError;
        __block NSDictionary* syncUUIDData;
        
        // get uuid data from server
        [self _getSyncDataFromServer:archiveId completion:^(NSDictionary *uuidData, NSError *error) {
            syncUUIDData = uuidData;
            syncDataError = error;
            
            dispatch_semaphore_signal(sema);
        }];
        
        // wait for network
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        
        if(syncDataError) {
            NSLog(@"Failed to load uuid_data: %@", syncDataError);
            
            self.lastError = [self _syncErrorWithCode:SyncServerErrorNetworkError underlyingError:syncDataError];
            
            [self _notifyDelegateOnSyncError:self.lastError];
            [self _finalizeSync];
        }
        else {
            // merge uuidData
            if(syncUUIDData) {
                [self.synchronizedItemIDs addEntriesFromDictionary:syncUUIDData];
            }
            
            // prepare assets for sync
            NSError* error;
            NSArray* syncAssets = [self _collectAssetsForSync:&error];
            
            if(error) {
                NSLog(@"Failed to enumerate assets for sync. Error = %@", error);
            }
            
            if(syncAssets) {
                [self _addAssetsIntoUploadQueues:syncAssets];
            }
        }
        
        // end background task
        [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
    });
}

- (void)cancel {
    // start background task
    __block UIBackgroundTaskIdentifier backgroundTask;
    backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"-[SyncServer cancel] expiration handler fired.");
        
        [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
        backgroundTask = UIBackgroundTaskInvalid;
    }];
    
    dispatch_async(self.operationQueue, ^{
        if(self.syncing) {
            [self _notifyDelegateOnSyncCancel];
            [self _finalizeSync];
            
            NSMutableArray *allTasks = [@[ self.foregroundUploadSession.tasks, self.backgroundUploadSession.tasks ] valueForKeyPath:@"@unionOfArrays.self"];
            
            // cancel all tasks
            for(NSURLSessionTask* task in allTasks) {
                [task cancel];
            }
        } else {
            NSLog(@"Not syncing.");
        }
        
        // end background task
        [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
    });
}

- (void)setAllowsCellularAccess:(BOOL)allowsCellularAccess {
    @synchronized(self) {
        if(_allowsCellularAccess == allowsCellularAccess) {
            return;
        }
        
        _allowsCellularAccess = allowsCellularAccess;
        
        // start background task
        __block UIBackgroundTaskIdentifier backgroundTask;
        backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
            NSLog(@"-[SyncServer setAllowsCellularAccess:] expiration handler fired.");
            
            [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
            backgroundTask = UIBackgroundTaskInvalid;
        }];
        
        dispatch_async(self.operationQueue, ^{
            // invalidate current session and create a new one with fresh configuration
            [self _recreateUploadSession];
            
            // end background task
            [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
        });
    }
}

#pragma mark - Notifications

- (void)_handleApplicationWillEnterForeground:(NSNotification *)notification {
    dispatch_async(self.operationQueue, ^{
        // reset flag that indicates that app was launched in background
        // to handle background session events
        self.didLaunchInBackground = NO;
        
        if(self.syncing) {
            NSLog(@"Application will enter foreground. Pump up foreground queue.");
            
            // start another background task
            [self _startBackgroundExecutionTask];
            
            // check for camera roll updates when app returns from suspension
            [self _checkForCameraRollUpdates];
            
            // this is pretty much noop if above works out
            [self _scheduleNextForegroundUploadsBatch];
        }
    });
}

#pragma mark - NSURLSession management

- (NSString *)backgroundURLSessionID {
    if(!_backgroundURLSessionID) {
        NSString* uuid = [NSUUID UUID].UUIDString.lowercaseString;
        _backgroundURLSessionID = [NSString stringWithFormat:@"%@.%@", kSyncServerBackgroundURLSessionID, uuid];
    }
    return _backgroundURLSessionID;
}

- (void)_setupURLSessions {
    // create background configuration for upload session
    NSURLSessionConfiguration* backgroundConfiguration = [self _backgroundSessionConfigurationWithIdentifier:self.backgroundURLSessionID];
    
    // create foreground configuration for upload session
    NSURLSessionConfiguration *foregroundConfiguration = [self _foregroundSessionConfiguration];
    
    // create background upload session
    self.backgroundUploadSession = [self _uploadSessionWithConfiguration:backgroundConfiguration];
    
    // create foreground upload session
    self.foregroundUploadSession = [self _uploadSessionWithConfiguration:foregroundConfiguration];
    
    // define weak self to use in blocks below
    __weak __typeof(self) weakSelf = self;
    
    //
    // setup background NSURLSession block handlers
    //
    [self.backgroundUploadSession setDidFinishEventsForBackgroundURLSessionBlock:^(NSURLSession *session) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if(!strongSelf) {
            return;
        }
        
        dispatch_async(strongSelf.operationQueue, ^{
            NSLog(@"Finished URLSession events processing.");
            [strongSelf _handleURLSessionDidFinishEvents:session];
        });
    }];
    
    [self.backgroundUploadSession setTaskDidCompleteBlock:^(NSURLSession *session, NSURLSessionTask *task, NSError *error) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if(!strongSelf) {
            return;
        }
        
        dispatch_async(strongSelf.operationQueue, ^{
            [strongSelf _backgroundURLSessionTaskDidComplete:task session:session error:error];
        });
    }];
    
    //
    // setup foreground NSURLSession block handlers
    //
    [self.foregroundUploadSession setTaskDidCompleteBlock:^(NSURLSession *session, NSURLSessionTask *task, NSError *error) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if(!strongSelf) {
            return;
        }
        
        dispatch_async(strongSelf.operationQueue, ^{
            [strongSelf _foregroundURLSessionTaskDidComplete:task session:session error:error];
        });
    }];
    
    NSLog(@"Create background NSURLSession with ID = %@, allowsCellularAccess = %@", backgroundConfiguration.identifier, backgroundConfiguration.allowsCellularAccess ? @"YES" : @"NO");
    NSLog(@"Create foreground NSURLSession, allowsCellularAccess = %@", foregroundConfiguration.allowsCellularAccess ? @"YES" : @"NO");
}

- (void)_recreateUploadSession {
    // Regenerate session ID
    // Simply set it to nil and it will be regenerated next time we call getter
    self.backgroundURLSessionID = nil;
    
    // reset uploading IDs
    [self.foregroundUploadingItemIDs removeAllObjects];
    [self.backgroundUploadingItemIDs removeAllObjects];

    // save cache
    [self _writeState];
    
    // invalid and cancel current session with all tasks
    // this will fire a squall of taskDidComplete with cancel error
    [self _invalidateURLUploadSessions];
    
    // setup new NSURLSession
    [self _setupURLSessions];
    
    // Schedule uploads
    if(self.syncing) {
        NSLog(@"Resume upload after changing session configuration");
        
        [self _scheduleNextForegroundUploadsBatchIfInForeground];
        [self _scheduleNextBackgroundUploadsBatch];
    }
}

- (void)_invalidateURLUploadSessions {
    // reset background session block handlers
    [self.backgroundUploadSession setTaskDidCompleteBlock:nil];
    [self.backgroundUploadSession setDidFinishEventsForBackgroundURLSessionBlock:nil];
    
    // reset foreground session block handlers
    [self.foregroundUploadSession setTaskDidCompleteBlock:nil];
    
    NSLog(@"Invalidate background NSURLSession with ID = %@.", self.backgroundUploadSession.session.configuration.identifier);
    NSLog(@"Invalidate foreground NSURLSession.");
    
    // log when sessions really became invalid
    // debug purpose only
    [self.backgroundUploadSession setSessionDidBecomeInvalidBlock:^(NSURLSession *session, NSError *error) {
        NSLog(@"Background NSURLSession did become invalid: %@. Error = %@", session.configuration.identifier, error);
    }];
    
    [self.foregroundUploadSession setSessionDidBecomeInvalidBlock:^(NSURLSession *session, NSError *error) {
        NSLog(@"Foreground NSURLSession did become invalid. Error = %@", error);
    }];
    
    // invalidate sessions and cancel all tasks
    [self.backgroundUploadSession invalidateSessionCancelingTasks:YES];
    [self.foregroundUploadSession invalidateSessionCancelingTasks:YES];
    
    // forget invalidated sessions
    self.backgroundUploadSession = nil;
    self.foregroundUploadSession = nil;
}

- (NSURLSessionConfiguration *)_foregroundSessionConfiguration {
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    sessionConfiguration.allowsCellularAccess = self.allowsCellularAccess;
    
    return sessionConfiguration;
}

- (NSURLSessionConfiguration*)_backgroundSessionConfigurationWithIdentifier:(NSString*)identifier {
    NSURLSessionConfiguration* sessionConfiguration;

    if([NSURLSessionConfiguration respondsToSelector:@selector(backgroundSessionConfigurationWithIdentifier:)]) { // iOS 8
        sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:identifier];
    } else { // iOS 7
        sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfiguration:identifier];
    }
    
    sessionConfiguration.allowsCellularAccess = self.allowsCellularAccess;
    
    return sessionConfiguration;
}

- (AFURLSessionManager*)_uploadSessionWithConfiguration:(NSURLSessionConfiguration*)configuration {
    // create URLSession manager
    AFURLSessionManager* sessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    // allow raw response
    sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    // Workaround system bug when upload tasks may not be created when in background
    sessionManager.attemptsToRecreateUploadTasksForBackgroundSessions = YES;
    
    return sessionManager;
}

- (void)_addURLSessionCompletionHandler:(void(^)())completionHandler forSession:(NSString *)identifier {
    dispatch_async(self.operationQueue, ^{
        if([self.URLSessionCompletionHandlers objectForKey:identifier]) {
            NSLog(@"Error: Got multiple handlers for a single session identifier %@. This should not happen.", identifier);
        }
        
        [self.URLSessionCompletionHandlers setObject:completionHandler forKey:identifier];
    });
}

- (void)_callURLSessionCompletionHandlerForSession:(NSString *)identifier {
    dispatch_async(self.operationQueue, ^{
        dispatch_sync(dispatch_get_main_queue(), ^{
            void(^handler)(void) = [self.URLSessionCompletionHandlers objectForKey:identifier];
            
            if(handler) {
                [self.URLSessionCompletionHandlers removeObjectForKey:identifier];
                NSLog(@"Calling completion handler for session %@", identifier);
                
                handler();
            }
        });
    });
}

- (void)handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler {
    NSLog(@"Handle events for background URL session: %@.", identifier);
    
    // set the flag that indicates that app was launched in background
    // to handle background session events
    dispatch_async(self.operationQueue, ^{
        self.didLaunchInBackground = YES;
    });
    
    [self _addURLSessionCompletionHandler:completionHandler forSession:identifier];
    [self restoreSessionFromCache];
}

- (void)_foregroundURLSessionTaskDidComplete:(NSURLSessionTask *)task session:(NSURLSession *)session error:(NSError *)error {
    // make sure we still sync
    if(!self.syncing) {
        return;
    }
    
    NSString* assetID = task.taskDescription;
    NSHTTPURLResponse* HTTPResponse = (NSHTTPURLResponse*)task.response;
    SyncAsset* item = [self _assetFromForegroundQueueByID:assetID];
    
    // Drop items missing in queue
    if(!item) {
        NSLog(@"Item with ID = %@ is not found in current queue.", assetID);
        return;
    }
    
    // unmark item as being uploaded
    [self.foregroundUploadingItemIDs removeObject:assetID];
    
    // Simply skip if request was cancelled
    if(error.code == NSURLErrorCancelled) {
        [self _writeState];
        return;
    }
    
    // check response
    if(HTTPResponse.statusCode == 200) { // OK
        [self _foregroundSyncDidFinishForAsset:item task:task error:error];
    }
    else {
        [self _foregroundSyncDidFailForAsset:item task:task error:error];
    }
}

- (void)_backgroundURLSessionTaskDidComplete:(NSURLSessionTask *)task session:(NSURLSession *)session error:(NSError *)error {
    NSString* assetID = task.taskDescription;
    NSHTTPURLResponse* HTTPResponse = (NSHTTPURLResponse*)task.response;
    
    // remove multipart file after upload
    NSString *multiPartFilePath = REMultipartRequestFilePath(assetID);
    NSError *fileError;
    if(![[NSFileManager defaultManager] removeItemAtPath:multiPartFilePath error:&fileError]) {
        NSLog(@"Cannot remove multipart file: %@", fileError);
    }
    
    // make sure we still sync
    if(!self.syncing) {
        return;
    }
    
    // Drop items missing in queue
    SyncAsset* item = [self _assetFromBackgroundQueueByID:assetID];
    if(!item) {
        NSLog(@"Item with ID = %@ is not found in current queue.", assetID);
        return;
    }
    
    // unmark item as being uploaded
    [self.backgroundUploadingItemIDs removeObject:assetID];
    
    // Simply skip if request was cancelled
    if(error.code == NSURLErrorCancelled) {
        [self _writeState];
        return;
    }
    
    // check response
    if(HTTPResponse.statusCode == 200) { // OK
        [self _backgroundSyncDidFinishForAsset:item task:task error:error];
    }
    else {
        [self _backgroundSyncDidFailForAsset:item task:task error:error];
    }
}

- (void)_handleURLSessionDidFinishEvents:(NSURLSession *)session {
    NSString *identifier = session.configuration.identifier;
    NSLog(@"Background URL session finished events: %@.", identifier);
    
    if(identifier) {
        // Call the handler we stored in -application:handleEventsForBackgroundURLSession:
        [self _callURLSessionCompletionHandlerForSession:identifier];
    }
}

#pragma mark - API

- (NSString*)_APIEndpoint:(NSString*)endpoint parameters:(NSDictionary*)parameters {
    if(parameters) {
        for(NSString* key in parameters) {
            NSString* replace = parameters[key];
            NSString* searchString = [NSString stringWithFormat:@"{%@}", key];
            endpoint = [endpoint stringByReplacingOccurrencesOfString:searchString withString:replace];
        }
    }
    return endpoint;
}

- (NSString*)_URLForAPIEndpoint:(NSString*)endpoint parametes:(NSDictionary*)parameters {
    return [kSyncAPIURL stringByAppendingString:[self _APIEndpoint:endpoint parameters:parameters]];
}

- (NSURLSessionDataTask*)_getSyncDataFromServer:(NSString*)archiveId completion:(void(^)(NSDictionary* uuidData, NSError* error))completion {
    NSParameterAssert(archiveId);
    NSParameterAssert(completion);
    
    NSDictionary* replace = @{
        @"archive_id": archiveId,
        @"device_uuid": self.deviceUUID
    };
    NSString* endpoint = [self _APIEndpoint:kSyncServerGetSyncDataEndpoint parameters:replace];
    
    // update serializer cookies
    NSDictionary* cookies = [self _cookieHeaders];
    for(NSString* key in cookies) {
        [self.APISession.requestSerializer setValue:cookies[key] forHTTPHeaderField:key];
    }
    
    return [self.APISession GET:endpoint parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if([responseObject isKindOfClass:NSDictionary.class]) {
            completion(responseObject, nil);
            return;
        }
        
        NSError* error = [self _syncErrorWithCode:SyncServerErrorInvalidUUIDData underlyingError:nil];
        
        completion(nil, error);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSError* err = [self _syncErrorWithCode:SyncServerErrorNetworkError underlyingError:error];
        
        completion(nil, err);
    }];
}

#pragma mark - Cache

- (NSString*)_filePathForState {
    static NSString* filePath;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString* applicationSupportDir = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) lastObject];
        NSString* storeDir = [applicationSupportDir stringByAppendingPathComponent:@"com.reflectera.syncserver"];
        
        // make sure directory exists
        [[NSFileManager defaultManager] createDirectoryAtPath:storeDir
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
        
        // craft file path
        filePath = [storeDir stringByAppendingPathComponent:@"syncstate.plist"];
    });
    
    return filePath;
}

- (BOOL)_writeState {
    NSString* filePath = [self _filePathForState];
    NSMutableDictionary* cacheDictionary = [NSMutableDictionary new];
    
    cacheDictionary[kSyncCacheBackgroundURLSessionID] = self.backgroundURLSessionID;
    cacheDictionary[kSyncCacheForegroundItemQueueKey] = self.foregroundItemQueue;
    cacheDictionary[kSyncCacheForegroundUploadingItemIDsKey] = self.foregroundUploadingItemIDs;
    cacheDictionary[kSyncCacheBackgroundItemQueueKey] = self.backgroundItemQueue;
    cacheDictionary[kSyncCacheBackgroundUploadingItemIDsKey] = self.backgroundUploadingItemIDs;
    cacheDictionary[kSyncCacheAllowsCellularAccess] = @(self.allowsCellularAccess);
    cacheDictionary[kSyncCacheSyncingKey] = @(self.syncing);
    cacheDictionary[kSyncCacheTotalItemCountKey] = @(self.totalItemCount);
    cacheDictionary[kSyncCacheCompletedItemCountKey] = @(self.completedItemCount);
    cacheDictionary[kSyncCacheSynchronizedItemIDsKey] = self.synchronizedItemIDs;

    if(self.deviceUUID) {
        cacheDictionary[kSyncCacheDeviceUUIDKey] = self.deviceUUID;
    }
    
    if(self.archiveID) {
        cacheDictionary[kSyncCacheArchiveIDKey] = self.archiveID;
    }
    
    if(self.lastError) {
        cacheDictionary[kSyncCacheLastErrorKey] = self.lastError;
    }
    
    //NSLog(@"Write cache.");
    
    // archive data
    NSData* data = [NSKeyedArchiver archivedDataWithRootObject:cacheDictionary];
    if(!data) {
        NSLog(@"Cannot serialize cache.");
        return NO;
    }
    
    // write data to file
    BOOL success = [data writeToFile:filePath atomically:YES];
    if(!success) {
        NSLog(@"Cannot save cache on disk.");
    }
    
    return success;
}

- (BOOL)_readState {
    NSString* filePath = [self _filePathForState];
    
    NSLog(@"Read sync cache.");
    
    // check if cache exists
    if(![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSLog(@"Cache is not created yet.");
        return NO;
    }
    
    // unarchive cache from disk
    NSDictionary* cacheDictionary;
    
    @try {
        cacheDictionary = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    }
    @catch (NSException *exception) {
        NSLog(@"Failed to unarchive cache: %@", exception);
    }
    
    if(!cacheDictionary) {
        NSLog(@"Cannot unarchive sync cache.");
        return NO;
    }
    
    // assign data
    self.totalItemCount = [cacheDictionary[kSyncCacheTotalItemCountKey] unsignedIntegerValue];
    self.completedItemCount = [cacheDictionary[kSyncCacheCompletedItemCountKey] unsignedIntegerValue];
    self.backgroundURLSessionID = cacheDictionary[kSyncCacheBackgroundURLSessionID];
    _allowsCellularAccess = [cacheDictionary[kSyncCacheAllowsCellularAccess] boolValue];
    self.deviceUUID = cacheDictionary[kSyncCacheDeviceUUIDKey];
    self.archiveID = cacheDictionary[kSyncCacheArchiveIDKey];
    self.syncing = [cacheDictionary[kSyncCacheSyncingKey] boolValue];
    self.lastError = cacheDictionary[kSyncCacheLastErrorKey];
    self.foregroundItemQueue = cacheDictionary[kSyncCacheForegroundItemQueueKey];
    self.foregroundUploadingItemIDs = cacheDictionary[kSyncCacheForegroundUploadingItemIDsKey];
    self.backgroundItemQueue = cacheDictionary[kSyncCacheBackgroundItemQueueKey];
    self.backgroundUploadingItemIDs = cacheDictionary[kSyncCacheBackgroundUploadingItemIDsKey];
    self.synchronizedItemIDs = cacheDictionary[kSyncCacheSynchronizedItemIDsKey];
    
    return YES;
}

- (void)_checkStateConsistency {
    NSLog(@"Check sync cache consistency.");

    [self _checkStateConsistency:self.foregroundUploadingItemIDs againstURLSessionTasks:self.foregroundUploadSession.uploadTasks];
    [self _checkStateConsistency:self.backgroundUploadingItemIDs againstURLSessionTasks:self.backgroundUploadSession.uploadTasks];
}

- (void)_checkStateConsistency:(NSMutableSet *)uploadingItemIDs againstURLSessionTasks:(NSArray *)tasks {
    NSSet* URLSessionItemIDs = [NSSet setWithArray:[tasks valueForKeyPath:@"taskDescription"]];
    
    NSMutableSet* missingItemIDs = [NSMutableSet setWithSet:uploadingItemIDs];
    [missingItemIDs minusSet:URLSessionItemIDs];
    
    if(missingItemIDs.count) {
        NSLog(@"Found invalid uploading assets with IDs = %@", missingItemIDs);
    }
    
    [uploadingItemIDs intersectSet:URLSessionItemIDs];
}

#pragma mark - Private methods

- (void)_startBackgroundExecutionTask {
    // start 3 minutes background task
    UIBackgroundTaskIdentifier previousBackgroundTask = self.backgroundTask;
    __block UIBackgroundTaskIdentifier newBackgroundTask = UIBackgroundTaskInvalid;
    
    newBackgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:newBackgroundTask];
        self.backgroundTask = UIBackgroundTaskInvalid;
    }];
    
    if(previousBackgroundTask != UIBackgroundTaskInvalid) {
        [[UIApplication sharedApplication] endBackgroundTask:previousBackgroundTask];
    }
    
    self.backgroundTask = newBackgroundTask;
    
    NSTimeInterval remainingTime = [UIApplication sharedApplication].backgroundTimeRemaining;
    if(remainingTime == DBL_MAX) {
        NSLog(@"Start background task for sync, time remaining: MAX");
    }
    else {
        NSLog(@"Start background task for sync, time remaining: %f", remainingTime);
    }
}

- (NSDictionary*)_cookieHeaders {
    NSHTTPCookieStorage* cookieStore = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSURL* mainURL = [NSURL URLWithString:kSyncAPIURL];
    NSArray* cookies = [cookieStore cookiesForURL:mainURL];
    return [NSHTTPCookie requestHeaderFieldsWithCookies:cookies];
}

- (NSError*)_syncErrorWithCode:(NSInteger)code underlyingError:(NSError*)underlyingError {
    static NSDictionary* strings;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        strings =
        @{
          @(SyncServerErrorNetworkError): @"Network error.",
          @(SyncServerErrorInvalidUUIDData): @"Server replied with malformed uuid_data.",
          @(SyncServerErrorAuthorizationFailed): @"Authorization failed.",
          @(SyncServerErrorPlanUpgradeRequired): @"Plan upgrade required."
        };
    });
    
    NSString* description = [strings objectForKey:@(code)];
    if(!description) {
        description = @"Unknown error";
    }
    
    NSMutableDictionary* userInfo = [NSMutableDictionary new];
    
    [userInfo setObject:description forKey:NSLocalizedDescriptionKey];
    
    if(underlyingError) {
        [userInfo setObject:underlyingError forKey:NSUnderlyingErrorKey];
    }
    
    return [NSError errorWithDomain:SyncServerErrorDomain code:code userInfo:userInfo];
}

- (ALAsset *)_ALAssetForURL:(NSURL *)assetURL error:(NSError **)outError {
    NSParameterAssert(assetURL);
    
    __block ALAsset *assetResult;
    
    // create semaphore
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    [[self.class sharedAssetsLibrary] assetForURL:assetURL
                                      resultBlock:^(ALAsset *alAsset) {
                                          assetResult = alAsset;
                                          dispatch_semaphore_signal(sema);
                                      }
                                     failureBlock:^(NSError *error) {
                                         NSLog(@"Cannot fetch asset for URL: %@. Error: %@", assetURL, error);
                                         
                                         if(outError) {
                                             *outError = error;
                                         }
                                         
                                         dispatch_semaphore_signal(sema);
                                     }];
    
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    
    return assetResult;
}

- (NSURLSessionUploadTask *)_uploadTaskWithStreamedContentsIntoFile:(NSURL *)multipartFileURL
                                                         serializer:(AFHTTPRequestSerializer *)requestSerializer
                                                            request:(NSMutableURLRequest *)multipartRequest
                                                              error:(NSError **)outError
{
    __block NSURLSessionUploadTask *uploadTask;
    
    // create semaphore
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    // serialize upload to file on disk
    [requestSerializer requestWithMultipartFormRequest:multipartRequest
                           writingStreamContentsToFile:multipartFileURL
                                     completionHandler:^(NSError *error) {
                                         if(error) {
                                             NSLog(@"Cannot serialize multipart request into file %@. Error = %@.", multipartFileURL.path, error);
                                             
                                             if(outError) {
                                                 *outError = error;
                                             }
                                             
                                             // release semaphore
                                             dispatch_semaphore_signal(sema);
                                             return;
                                         }
                                         
                                         // create upload task
                                         uploadTask = [self.backgroundUploadSession uploadTaskWithRequest:multipartRequest
                                                                                                 fromFile:multipartFileURL
                                                                                                 progress:nil
                                                                                        completionHandler:nil];
                                         
                                         // release semaphore
                                         dispatch_semaphore_signal(sema);
                                     }];
    
    // wait
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    
    return uploadTask;
}

- (NSURLSessionUploadTask *)_uploadTaskWithStreamedRequest:(NSMutableURLRequest *)multipartRequest
{
    return [self.foregroundUploadSession uploadTaskWithStreamedRequest:multipartRequest
                                                              progress:nil
                                                     completionHandler:nil];
}

- (NSURLSessionUploadTask *)_uploadTaskForAsset:(SyncAsset *)syncAsset
                     shouldUseBackgroundSession:(BOOL)shouldUseBackgroundSession
                                     parameters:(NSDictionary *)parameters
                                          error:(NSError **)outError
{
    // fetch ALAsset
    ALAsset *alAsset = [self _ALAssetForURL:syncAsset.assetURL error:outError];
    if(!alAsset) {
        return nil;
    }
    
    // create request serializer
    AFHTTPRequestSerializer* requestSerializer = [AFHTTPRequestSerializer serializer];
    
    // enable cookies
    requestSerializer.HTTPShouldHandleCookies = YES;
    
    // body constructor from asset input stream
    void(^bodyConstructor)(id<AFMultipartFormData> formData) = ^(id<AFMultipartFormData> formData) {
        AssetStream* stream = [AssetStream inputStreamWithAsset:alAsset];
        
        [formData appendPartWithInputStream:stream
                                       name:@"file"
                                   fileName:syncAsset.fileName
                                     length:alAsset.defaultRepresentation.size
                                   mimeType:syncAsset.mimeType];
    };
    
    // create a multipart form request
    NSString* endpointURL = [self _URLForAPIEndpoint:kSyncServerUploadDataEndpoint parametes:nil];
    NSMutableURLRequest *multipartRequest = [requestSerializer multipartFormRequestWithMethod:@"POST"
                                                                                    URLString:endpointURL
                                                                                   parameters:parameters
                                                                    constructingBodyWithBlock:bodyConstructor
                                                                                        error:outError];
    
    // check errors
    if(!multipartRequest) {
        return nil;
    }
    
    // set cookies
    NSDictionary* HTTPCookieHeaders = [self _cookieHeaders];
    for(NSString* name in HTTPCookieHeaders) {
        [multipartRequest setValue:HTTPCookieHeaders[name] forHTTPHeaderField:name];
    }
    
    // resolve and create task according to requested queue (foreground or background)
    NSURLSessionUploadTask* uploadTask;
    if(shouldUseBackgroundSession) {
        NSURL* multipartFileURL = [NSURL fileURLWithPath:REMultipartRequestFilePath(syncAsset.assetID)];
        
        uploadTask = [self _uploadTaskWithStreamedContentsIntoFile:multipartFileURL serializer:requestSerializer request:multipartRequest error:outError];
    }
    else {
        uploadTask = [self _uploadTaskWithStreamedRequest:multipartRequest];
    }
    
    // save assetID in taskDescription
    uploadTask.taskDescription = syncAsset.assetID;
    
    return uploadTask;
}

/**
 *  Find assets for synchronization.
 *  This method checks Photo library for videos and photos.
 *
 *  It filters out (skips) assets that are:
 *
 *  1. Already synchronized assets
 *  2. Assets that are already in foreground queue
 *  3. Assets that are already in background queue
 *
 *  @param completion a completion handler
 */
- (NSArray *)_collectAssetsForSync:(NSError **)outError {
    // create assets' URLs storage
    __block NSMutableArray* syncAssets = [NSMutableArray new];
    __block NSMutableDictionary* assetsMap = [NSMutableDictionary new];
    
    // prepare two sets of IDs for each queue
    __block NSSet *foregroundAssetIDs = [NSSet setWithArray:[self.foregroundItemQueue valueForKeyPath:@"assetID"]];
    __block NSSet *backgroundAssetIDs = [NSSet setWithArray:[self.backgroundItemQueue valueForKeyPath:@"assetID"]];
    
    // setup assets filter
    ALAssetsLibrary *assetsLibrary = [self.class sharedAssetsLibrary];
    ALAssetsGroupType assetsGroupType = ALAssetsGroupAlbum | ALAssetsGroupEvent | ALAssetsGroupSavedPhotos;
    ALAssetsFilter *assetsFilter = [ALAssetsFilter allAssets];
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    // find out available disk space
    NSError *fsError;
    NSDictionary *fsAttributes = [[NSFileManager defaultManager] attributesOfFileSystemForPath:NSTemporaryDirectory() error:&fsError];
    long long freeDiskSpace = [[fsAttributes objectForKey:NSFileSystemFreeSize] longLongValue];
    long long affordableDiskSpace = MAX(100 * 1024 * 1024, freeDiskSpace * 0.5);
    
    // enumerate assets' groups
    [assetsLibrary enumerateGroupsWithTypes:assetsGroupType usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        // check for the end of enumeration
        if(!group) {
            dispatch_semaphore_signal(sema);
            return;
        }
        
        // set assets filter
        [group setAssetsFilter:assetsFilter];
        
        // extract asset group information
        NSString* groupName = [group valueForProperty:ALAssetsGroupPropertyName];
        
        // enumerate assets in group
        [group enumerateAssetsUsingBlock:^(ALAsset* alAsset, NSUInteger index, BOOL *stop) {
            // check for the end of enumeration
            if(!alAsset) {
                return;
            }
            
            // get asset URL
            NSURL* assetURL = [alAsset valueForProperty:ALAssetPropertyAssetURL];
            
            // extract asset ID part from URL
            NSString* assetID = [SyncAsset assetIDFromURL:assetURL];
            
            // asset type
            NSString *assetType = [alAsset valueForProperty:ALAssetPropertyType];
            
            // Skip already synchronized assets
            if([self.synchronizedItemIDs objectForKey:assetID]) {
                return;
            }
            
            // Skip uploading assets
            if([foregroundAssetIDs containsObject:assetID] || [backgroundAssetIDs containsObject:assetID]) {
                return;
            }
            
            // Check if file is too large
            if([assetType isEqualToString:ALAssetTypeVideo] && alAsset.defaultRepresentation.size >= affordableDiskSpace) {
                NSString *fileSizeString = [NSByteCountFormatter stringFromByteCount:alAsset.defaultRepresentation.size countStyle:NSByteCountFormatterCountStyleBinary];
                NSString *freeDiskSpaceString = [NSByteCountFormatter stringFromByteCount:freeDiskSpace countStyle:NSByteCountFormatterCountStyleBinary];
                NSString *affordableDiskSpaceString = [NSByteCountFormatter stringFromByteCount:affordableDiskSpace countStyle:NSByteCountFormatterCountStyleBinary];
                NSLog(@"Video asset %@ is too big. File size: %@, free disk space: %@, affordable disk space: %@", assetID, fileSizeString, freeDiskSpaceString, affordableDiskSpaceString);
                return;
            }
            
            SyncAsset* syncAsset = [assetsMap objectForKey:assetID];
            
            if(!syncAsset) {
                // Create a lightweight object to hold asset info
                syncAsset = [SyncAsset new];
                syncAsset.assetURL = assetURL;
                syncAsset.assetID = [syncAsset.class assetIDFromURL:syncAsset.assetURL];
                syncAsset.assetExtension = [syncAsset.class assetExtensionFromURL:syncAsset.assetURL];
                syncAsset.mimeType = [syncAsset.class mimeTypeForAsset:alAsset];
                syncAsset.kind = SyncAssetKindUnknown;
                syncAsset.size = alAsset.defaultRepresentation.size;
                
                // determine the type of asset
                if([assetType isEqualToString:ALAssetTypePhoto]) {
                    syncAsset.kind = SyncAssetKindPhoto;
                }
                else if([assetType isEqualToString:ALAssetTypeVideo]) {
                    syncAsset.kind = SyncAssetKindVideo;
                }
                
                [syncAssets addObject:syncAsset];
                
                // save asset in memory
                [assetsMap setObject:syncAsset forKey:assetID];
            }
            
            // add album that asset is included into
            // can be multiple albums
            [syncAsset addGroup:groupName];
        }];
    } failureBlock:^(NSError *error) {
        if(outError) {
            *outError = error;
        }
        
        dispatch_semaphore_signal(sema);
    }];
    
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    
    return syncAssets;
}

/**
 *  This method adds assets into upload queues.
 *
 *  @param items an array of assets to enqueue
 */
- (void)_addAssetsIntoUploadQueues:(NSArray *)items {
    NSLog(@"Added %ld assets for synchronization.", (unsigned long)items.count);
    
    // add more items to current counter
    self.totalItemCount += items.count;
    
    // create synchronization progress
    if(!self.progress) {
        self.progress = [NSProgress progressWithTotalUnitCount:self.totalItemCount];
        
        // notify delegate when sync starts
        [self _notifyDelegateOnSyncUpload];
    }
    else {
        self.progress.totalUnitCount = self.totalItemCount;
    }
    
    // distribute items between foreground and background queues
    for(SyncAsset *item in items) {
        if(item.kind == SyncAssetKindPhoto) {
            [self.foregroundItemQueue addObject:item];
        } else {
            [self.backgroundItemQueue addObject:item];
        }
    }
    
    // save items to cache
    [self _writeState];
    
    // schedule uploads
    [self _scheduleNextForegroundUploadsBatchIfInForeground];
    [self _scheduleNextBackgroundUploadsBatch];
}

- (void)_checkForCameraRollUpdates {
    if(!self.syncing) {
        return;
    }
    
    if(self.foregroundItemQueue.count > 0) {
        NSLog(@"Not looking for updates in camera roll until foreground queue is empty.");
        return;
    }
    
    // prepare assets for sync
    NSError* error;
    NSArray* syncAssets = [self _collectAssetsForSync:&error];
    
    NSLog(@"Found %lu new assets in camera roll.", (unsigned long)syncAssets.count);
    
    if(error) {
        NSLog(@"Failed to enumerate assets for sync. Error = %@", error);
    }
    
    if(syncAssets.count > 0) {
        [self _addAssetsIntoUploadQueues:syncAssets];
    }
}

- (void)_finalizeSync {
    [self _notifyDelegateOnSyncFinish];
    
    self.totalItemCount = 0;
    self.completedItemCount = 0;
    self.progress = nil;
    self.syncing = NO;
    
    // clean up in case of emergency exit
    [self.foregroundItemQueue removeAllObjects];
    [self.foregroundUploadingItemIDs removeAllObjects];
    
    [self.backgroundItemQueue removeAllObjects];
    [self.backgroundUploadingItemIDs removeAllObjects];
    
    [self _writeState];
}

- (void)_advanceSyncProgress {
    // update counter
    self.completedItemCount++;
    
    // update progress
    self.progress.completedUnitCount = self.completedItemCount;
}

- (NSDictionary*)_prepareRequestParametersForItem:(SyncAsset*)item {
    NSData* serializedTagsData = [NSJSONSerialization dataWithJSONObject:item.groups options:0 error:nil];
    NSString* serializedTagsJSON = [[NSString alloc] initWithData:serializedTagsData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary* parameters = [NSMutableDictionary new];
    NSString* uuidData = @"false";
    
    // calculate remaining number of items for synchronization
    NSInteger remainingItemCount = self.totalItemCount - self.completedItemCount;
    
    // add uuid_data to request from time to time
    if(self.completedItemCount % 10 == 0 || remainingItemCount == 1) {
        // make a copy of synchronized items
        NSMutableDictionary* uuidDictionary = [self.synchronizedItemIDs mutableCopy];
        
        // add item that is about to be synced
        [uuidDictionary setValue:@YES forKey:item.assetID];
        
        // serialize uuid data in JSON
        NSData* serializedUUIDData = [NSJSONSerialization dataWithJSONObject:uuidDictionary options:0 error:nil];
        if(!serializedUUIDData) {
            NSLog(@"Cannot serialize uuid_data?");
        }
        
        // convert uuid data JSON to string
        if(serializedUUIDData) {
            uuidData = [[NSString alloc] initWithData:serializedUUIDData encoding:NSUTF8StringEncoding];
        }
    }
    
    parameters[@"platform"] = @"iOS";
    parameters[@"archive_id"] = self.archiveID;
    parameters[@"album_id"] = @"false";
    parameters[@"ext"] = item.assetExtension;
    parameters[@"uuid_data"] = uuidData;
    parameters[@"item_uuid"] = item.assetID;
    parameters[@"device_uuid"] = self.deviceUUID;
    parameters[@"tags"] = serializedTagsJSON;
    
    return parameters;
}

#pragma mark - Background Tasks

- (void)_backgroundSyncDidFinishForAsset:(SyncAsset *)asset task:(NSURLSessionTask *)task error:(NSError *)error {
    NSLog(@"BACKGROUND: Asset %@ has been uploaded.", asset.assetID);
    
    // mark item as synchronized
    [self.synchronizedItemIDs setObject:@YES forKey:asset.assetID];
    
    // remove object from queue
    [self.backgroundItemQueue removeObject:asset];
    
    // save cache
    [self _writeState];
    
    // update counter
    [self _advanceSyncProgress];
    
    // schedule next upload
    [self _scheduleNextBackgroundUploadsBatch];
}

- (void)_backgroundSyncDidFailForAsset:(SyncAsset *)asset task:(NSURLSessionTask *)task error:(NSError *)error {
    NSHTTPURLResponse* HTTPResponse = (NSHTTPURLResponse*)task.response;
    
    NSLog(@"Error uploading item %@. HTTP code = %ld. Error = %@", asset.assetID, (long)HTTPResponse.statusCode, error);
    
    // remove object from queue
    [self.backgroundItemQueue removeObject:asset];
    
    // save cache
    [self _writeState];
    
    // update counter
    [self _advanceSyncProgress];
    
    SyncServerError errorCode = SyncServerErrorNetworkError;
    BOOL shouldCancelSync = NO;
    
    if(HTTPResponse.statusCode == 401) { // Authorization failed
        errorCode = SyncServerErrorAuthorizationFailed;
        shouldCancelSync = YES;
    } else if(HTTPResponse.statusCode == 426) { // Plan upgrade required
        errorCode = SyncServerErrorPlanUpgradeRequired;
        shouldCancelSync = YES;
    }
    
    // save last error
    self.lastError = [self _syncErrorWithCode:errorCode underlyingError:error];
    
    // send error to delegate
    [self _notifyDelegateOnSyncError:self.lastError];
    
    // cancel sync when bad HTTP code received.
    if(shouldCancelSync) {
        [self cancel];
        return;
    }
    
    // schedule next upload
    [self _scheduleNextBackgroundUploadsBatch];
}

- (NSArray *)_scheduleNextBackgroundUploadsBatch {
    NSMutableArray *items = [NSMutableArray new];
    
    NSInteger active = self.backgroundUploadingItemIDs.count;
    NSInteger count = kMaxConcurrentBackgroundUploads - active;
    
    for(NSInteger i = 0; i < count; i++) {
        // schedule next upload
        SyncAsset* item = [self _scheduleNextBackgroundUpload];
        if(item) {
            [items addObject:item];
        }
    }
    
    if(items.count > 0) {
        NSLog(@"BACKGROUND: Scheduled %ld batch of assets for upload.", (unsigned long)items.count);
    }
    
    // no items for upload?
    if(self.foregroundItemQueue.count == 0 && self.backgroundItemQueue.count == 0) {
        // notify delegate when sync stopped
        [self _finalizeSync];
    }
    
    return items;
}

- (SyncAsset*)_scheduleNextBackgroundUpload {
    SyncAsset* item = [self _nextCandidateAssetForSyncFromQueue:self.backgroundItemQueue uploadingItems:self.backgroundUploadingItemIDs];
    if(!item) {
        return nil;
    }
    
    // create upload task for asset
    NSError* error;
    NSDictionary* parameters = [self _prepareRequestParametersForItem:item];
    NSURLSessionUploadTask *uploadTask = [self _uploadTaskForAsset:item
                                        shouldUseBackgroundSession:YES
                                                        parameters:parameters
                                                             error:&error];
    
    // Handle case when asset is missing or something went wrong...
    if(error || !uploadTask) {
        NSLog(@"Failed to create upload task for asset = %@, error = %@", item.assetID, error);
        [self.backgroundItemQueue removeObject:item];
        
        return [self _scheduleNextBackgroundUpload];
    }
    
    // mark item as uploading
    [self.backgroundUploadingItemIDs addObject:item.assetID];
    
    // save cache
    [self _writeState];
    
    // start upload
    [uploadTask resume];
    
    NSLog(@"BACKGROUND: Upload %@, upload.taskId = %ld.", item.assetID, (unsigned long)uploadTask.taskIdentifier);
    
    return item;
}

- (SyncAsset*)_assetFromBackgroundQueueByID:(NSString*)assetID {
    NSUInteger index = [self.backgroundItemQueue indexOfObjectPassingTest:^BOOL(SyncAsset* item, NSUInteger idx, BOOL *stop) {
        if([item.assetID isEqualToString:assetID]) {
            *stop = YES;
            return YES;
        }
        return NO;
    }];
    
    if(index == NSNotFound) {
        return nil;
    }
    
    return [self.backgroundItemQueue objectAtIndex:index];
}

#pragma mark - Foreground tasks

- (void)_foregroundSyncDidFinishForAsset:(SyncAsset *)asset task:(NSURLSessionTask *)task error:(NSError *)error {
    NSLog(@"FOREGROUND: Asset %@ has been uploaded.", asset.assetID);
    
    // mark item as synchronized
    [self.synchronizedItemIDs setObject:@YES forKey:asset.assetID];
    
    // remove object from queue
    [self.foregroundItemQueue removeObject:asset];
    
    // save cache
    [self _writeState];
    
    // update counter
    [self _advanceSyncProgress];
    
    // schedule next upload
    if(!self.didLaunchInBackground) {
        [self _scheduleNextForegroundUploadsBatch];
    }
    else {
        NSLog(@"FOREGROUND: Cannot continue uploads in foreground, app is running in background.");
    }
}

- (void)_foregroundSyncDidFailForAsset:(SyncAsset *)asset task:(NSURLSessionTask *)task error:(NSError *)error {
    NSHTTPURLResponse* HTTPResponse = (NSHTTPURLResponse*)task.response;
    
    NSLog(@"Error uploading item %@. HTTP code = %ld. Error = %@", asset.assetID, (long)HTTPResponse.statusCode, error);
    
    SyncServerError errorCode = SyncServerErrorNetworkError;
    BOOL shouldRetry = NO;
    BOOL shouldCancelSync = NO;
    BOOL exceedsRetryCount = asset.retryCount >= kMaxRetryCountForUploads;
    
    if(HTTPResponse.statusCode == 401) { // Authorization failed
        errorCode = SyncServerErrorAuthorizationFailed;
        shouldCancelSync = YES;
    }
    else if(HTTPResponse.statusCode == 426) { // Plan upgrade required
        errorCode = SyncServerErrorPlanUpgradeRequired;
        shouldCancelSync = YES;
    }
    else {
        shouldRetry = YES;
    }
    
    // save last error
    self.lastError = [self _syncErrorWithCode:errorCode underlyingError:error];
    
    if(exceedsRetryCount) {
        NSLog(@"FOREGROUND: Failed to upload %@ %ld times. Skip.", asset.assetID, (long)asset.retryCount);
    }
    
    // remove asset from queue if retry counter hits the limit
    if(!shouldRetry || exceedsRetryCount) {
        // remove object from queue
        [self.foregroundItemQueue removeObject:asset];
        
        // update counter
        [self _advanceSyncProgress];
    }
    else if(shouldRetry) {
        asset.retryCount++;
        
        NSLog(@"FOREGROUND: Retry upload for %@, attempt %ld", asset.assetID, (long)asset.retryCount);
    }
    
    // save cache
    [self _writeState];
    
    // send error to delegate
    [self _notifyDelegateOnSyncError:self.lastError];
    
    // cancel sync when bad HTTP code received.
    if(shouldCancelSync) {
        [self cancel];
        return;
    }
    
    // schedule next upload
    if(!self.didLaunchInBackground) {
        [self _scheduleNextForegroundUploadsBatch];
    }
    else {
        NSLog(@"FOREGROUND: Cannot continue uploads in foreground, app is running in background.");
    }
}

- (NSArray *)_scheduleNextForegroundUploadsBatchIfInForeground {
    if([UIApplication sharedApplication].applicationState != UIApplicationStateBackground) {
        return [self _scheduleNextForegroundUploadsBatch];
    }
    
    NSLog(@"FOREGROUND: Skip scheduling since app is in background.");
    
    return [NSArray array];
}

- (NSArray *)_scheduleNextForegroundUploadsBatch {
    NSMutableArray *items = [NSMutableArray new];
    
    NSInteger active = self.foregroundUploadingItemIDs.count;
    NSInteger count = kMaxConcurrentForegroundUploads - active;
    
    for(NSInteger i = 0; i < count; i++) {
        // schedule next upload
        SyncAsset* item = [self _scheduleNextForegroundUpload];
        if(item) {
            [items addObject:item];
        }
    }
    
    if(items.count > 0) {
        NSLog(@"FOREGROUND: Scheduled %ld batch of assets for upload.", (unsigned long)items.count);
    }
    
    // no items for upload?
    if(self.foregroundItemQueue.count == 0 && self.backgroundItemQueue.count == 0) {
        // notify delegate when sync stopped
        [self _finalizeSync];
    }
    
    return items;
}

- (SyncAsset*)_scheduleNextForegroundUpload {
    SyncAsset* item = [self _nextCandidateAssetForSyncFromQueue:self.foregroundItemQueue uploadingItems:self.foregroundUploadingItemIDs];
    if(!item) {
        return nil;
    }
    
    // create upload task for asset
    NSError* error;
    NSDictionary* parameters = [self _prepareRequestParametersForItem:item];
    NSURLSessionUploadTask *uploadTask = [self _uploadTaskForAsset:item
                                        shouldUseBackgroundSession:NO
                                                        parameters:parameters
                                                             error:&error];
    
    // Handle case when asset is missing or something went wrong...
    if(error || !uploadTask) {
        NSLog(@"Failed to create upload task for asset = %@, error = %@", item.assetID, error);
        [self.foregroundItemQueue removeObject:item];
        
        return [self _scheduleNextForegroundUpload];
    }
    
    // mark item as uploading
    [self.foregroundUploadingItemIDs addObject:item.assetID];
    
    // save cache
    [self _writeState];
    
    // start upload
    [uploadTask resume];
    
    NSLog(@"FOREGROUND: Upload %@, upload.taskId = %ld.", item.assetID, (unsigned long)uploadTask.taskIdentifier);
    
    return item;
}

- (SyncAsset *)_nextCandidateAssetForSyncFromQueue:(NSArray *)queue uploadingItems:(NSSet *)uploadingItemIDs {
    for(SyncAsset* asset in queue) {
        if(![uploadingItemIDs containsObject:asset.assetID]) {
            return asset;
        }
    }
    return nil;
}

- (SyncAsset*)_assetFromForegroundQueueByID:(NSString*)assetID {
    NSUInteger index = [self.foregroundItemQueue indexOfObjectPassingTest:^BOOL(SyncAsset* item, NSUInteger idx, BOOL *stop) {
        if([item.assetID isEqualToString:assetID]) {
            *stop = YES;
            return YES;
        }
        return NO;
    }];
    
    if(index == NSNotFound) {
        return nil;
    }
    
    return [self.foregroundItemQueue objectAtIndex:index];
}

#pragma mark - Delegate helper methods

- (void)_notifyDelegateOnSyncStart {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"Delegate: onSyncStart");
        if([self.delegate respondsToSelector:@selector(syncServerDidStartSyncing:)]) {
            [self.delegate syncServerDidStartSyncing:self];
        }
    });
}

- (void)_notifyDelegateOnSyncUpload {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"Delegate: onSyncUpload");
        if([self.delegate respondsToSelector:@selector(syncServer:didStartUploadingWithProgress:)]) {
            [self.delegate syncServer:self didStartUploadingWithProgress:self.progress];
        }
    });
}

- (void)_notifyDelegateOnSyncFinish {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"Delegate: onSyncFinish");
        if([self.delegate respondsToSelector:@selector(syncServerDidFinishSyncing:)]) {
            [self.delegate syncServerDidFinishSyncing:self];
        }
    });
}

- (void)_notifyDelegateOnSyncCancel {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"Delegate: onSyncCancel");
        if([self.delegate respondsToSelector:@selector(syncServerDidCancelSyncing:)]) {
            [self.delegate syncServerDidCancelSyncing:self];
        }
    });
}

- (void)_notifyDelegateOnSyncError:(NSError*)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"Delegate: onSyncError");
        if([self.delegate respondsToSelector:@selector(syncServer:didFailWithError:)]) {
            [self.delegate syncServer:self didFailWithError:error];
        }
    });
}

@end
