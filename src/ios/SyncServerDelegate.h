//
//  SyncServerDelegate.h
//  Reflectera
//
//  Created by pronebird on 22/01/15.
//
//

#import <Foundation/Foundation.h>

@class SyncServer;

@protocol SyncServerDelegate<NSObject>

@optional
- (void)syncServerDidStartSyncing:(SyncServer*)server;
- (void)syncServerDidFinishSyncing:(SyncServer*)server;
- (void)syncServerDidCancelSyncing:(SyncServer*)server;
- (void)syncServer:(SyncServer*)server didStartUploadingWithProgress:(NSProgress*)progress;
- (void)syncServer:(SyncServer*)server didFailWithError:(NSError*)error;

@end
