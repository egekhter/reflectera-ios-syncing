//
//  AssetStream.h
//  Reflectera
//
//  Created by pronebird on 23/01/15.
//
//

#import <Foundation/Foundation.h>

@class ALAsset;

@interface AssetStream : NSInputStream

+ (instancetype)inputStreamWithAsset:(ALAsset*)asset;

- (id)initWithAsset:(ALAsset*)asset;

@end
